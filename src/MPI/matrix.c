#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

#include "mpi.h"

static int MATRIX_SIZE = 4; // default value, its going to be changed when matrices are read

void copy_matrix_content(
    double * src, const int size_src, const int src_i_offset, const int src_j_offset,
    double * dst, const int size_dst, const int dst_i_offset, const int dst_j_offset,
    const int num_rows, const int num_cols
) {
    int i, j;

    for (i = 0; i < num_rows; i++) {
        for (j = 0; j < num_cols; j++) {
            dst[(i + dst_i_offset) * size_dst + (j + dst_j_offset)] = src[(i + src_i_offset) * size_src + (j + src_j_offset)];
        }
    }
}

// calculate the cofactor of element (row,col)
static int calc_minor(double * src, double * dst, const int row, const int col, const int order, const int dst_order) {
    // indicate which col and row is being copied to dst
    int col_count = 0, row_count = 0;
    int i, j;

    for(i = 0; i < order; i++ ) {
        if(i != row) {
            col_count = 0;
            for(j = 0; j < order; j++ ) {
                // when j is not the element
                if(j != col)
                {
                    dst[row_count * dst_order + col_count] = src[i * order +j];
                    col_count++;
                }
            }
            row_count++;
        }
    }

    return 1;
}

// Calculate the determinant recursively.
static double determinant(double * matrix, int order) {
    double det = 0;
    double * minor;
    int i;

    // order must be >= 1
    // stop the recursion when matrix is a single element
    if(order == 1) {
        return matrix[0];
    }

    minor = (double *) malloc((order - 1) * (order - 1) * sizeof(double));

    for(i = 0; i < order; i++) {
        calc_minor(matrix, minor, 0, i, order, order-1);
        det += (i%2 == 1 ? -1.0 : 1.0) * matrix[i] * determinant(minor,order-1);
    }

    free(minor);

    return det;
}

void inverse(double * A, double * dst, const int order) {
    int i, j;
    double * minor;
    double det = 1.0/determinant(A,order);

    minor = (double *) malloc((order - 1) * (order - 1) * sizeof(double));

    for(j = 0; j < order; j++) {
        for(i = 0; i < order; i++) {
            // get the co-factor (matrix) of A(j,i)
            calc_minor(A, minor, j, i, order, order-1);
            dst[i * order + j] = det * determinant(minor, order-1);
            if( (i+j)%2 == 1)
                dst[i * order + j] *= -1;
        }
    }

    free(minor);
}

void inverse_L(double * L, double * L_inv, const int order) {
    int i, j;
    double * e;
    double * col;

    e = (double *) malloc(order * sizeof(double));
    col = (double *) malloc(order * sizeof(double));

    for (j = 0; j < order; j++) {
        memset(e, order, order * sizeof(double));
        e[j] = 1;
        forward_substitution(L, e, col, order);
        for (i = 0; i < order; i++) {
            L_inv[i*order + j] = col[i];
        }
    }

    free(e);
    free(col);
}

void inverse_U(double * U, double * U_inv, const int order) {
    int i, j;
    double * e;
    double * col;

    e = (double *) malloc(order * sizeof(double));
    col = (double *) malloc(order * sizeof(double));

    for (j = 0; j < order; j++) {
        memset(e, order, order * sizeof(double));
        e[j] = 1;
        backward_substitution(U, e, col, order);
        for (i = 0; i < order; i++) {
            U_inv[i*order + j] = col[i];
        }
    }

    free(e);
    free(col);
}

static void diffusion(
    double * matrix,
    const int i_offset,
    const int j_offset,
    const int matrix_size,
    const int count,
    const int root
) {
    int i;

    for (i = 0; i < count; i++) {
        MPI_Bcast(&matrix[(i + i_offset) * matrix_size + j_offset], count, MPI_DOUBLE, root, MPI_COMM_WORLD);
    }
}

static void LU_factorization_block(
    double * A, double * L, double * U,
    const int A_size, const int L_size, const int U_size,
    const int block_size,
    const int A_i_offset, const int A_j_offset,
    const int L_i_offset, const int L_j_offset,
    const int U_i_offset, const int U_j_offset
) {
    int i, j, k;

    for (k = 0; k < block_size - 1; k++) {
        for (i = k+1; i < block_size; i++) {
            A[(i+A_i_offset) * A_size + (k+A_j_offset)] /= A[(k+A_i_offset) * A_size + (k+A_j_offset)];
        }
        for (j = k+1; j < block_size; j++) {
            for (i = k+1; i < block_size; i++) {
                A[(i+A_i_offset) * A_size + (j+A_j_offset)] -= A[(i+A_i_offset) * A_size + (k+A_j_offset)] * A[(k+A_i_offset) * A_size + (j+A_j_offset)];
            }
        }
    }


    // Assignments of L and U
    for (i = 0; i < block_size; i++) {
        L[(i+L_i_offset) * L_size + (i+L_j_offset)] = 1;
        for (j = 0; j < i; j++) {
            L[(i+L_i_offset) * L_size + (j+L_j_offset)] = A[(i+A_i_offset) * A_size + (j+A_j_offset)];
        }
        for (j = i; j < block_size; j++) {
            U[(i+U_i_offset) * U_size + (j+U_j_offset)] = A[(i+A_i_offset) * A_size + (j+A_j_offset)];
        }
    }
}

void LU_factorization_serial(double * A, double * L, double * U, const int size) {
    int i, j, k;

    for (k = 0; k < size - 1; k++) {
        for (i = k+1; i < size; i++) {
            A[i * size + k] /= A[k * size + k];
        }
        for (j = k+1; j < size; j++) {
            for (i = k+1; i < size; i++) {
                A[i * size + j] -= A[i * size + k] * A[k * size + j];
            }
        }
    }


    // Assignments of L and U
    for (i = 0; i < size; i++) {
        L[i * size + i] = 1;
        for (j = 0; j < i; j++) {
            L[i * size + j] = A[i * size + j];
        }
        for (j = i; j < size; j++) {
            U[i * size + j] = A[i * size + j];
        }
    }
}

void matrix_mult(double const * A, double const * B, double * C) {
    int i, j, k;

    for (i = 0; i < MATRIX_SIZE; i++) {
        for (j = 0; j < MATRIX_SIZE; j++) {
            C[i * MATRIX_SIZE + j] = 0;
        }
    }

    for (i = 0; i < MATRIX_SIZE; i++) {
        for (k = 0; k < MATRIX_SIZE; k++) {
            for (j = 0; j < MATRIX_SIZE; j++) {
                C[i * MATRIX_SIZE + j] += A[i * MATRIX_SIZE + k] * B[k * MATRIX_SIZE + j];
            }
        }
    }
}

static void matrix_mult_block(
    double const * A,
    double const * B,
    double * C,
    const int size,
    const int A_size,
    const int B_size,
    const int C_size,
    const int a_i_offset,
    const int a_j_offset,
    const int b_i_offset,
    const int b_j_offset,
    const int c_i_offset,
    const int c_j_offset
) {
    int i, j, k;
    double aux = 0;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            aux = 0;
            for (k = 0; k < size; k++) {
                 aux +=
                    A[(i + a_i_offset) * A_size + (k + a_j_offset)] *
                    B[(k + b_i_offset) * B_size + (j + b_j_offset)];
            }
            C[(i + c_i_offset) * C_size + (j + c_j_offset)] = aux;
        }
    }
}

static void matrix_sub_block(
    double const * A,
    double const * B,
    double * C,
    const int size,
    const int A_size,
    const int B_size,
    const int C_size,
    const int a_i_offset,
    const int a_j_offset,
    const int b_i_offset,
    const int b_j_offset,
    const int c_i_offset,
    const int c_j_offset
) {
    int i, j;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            C[(i + c_i_offset) * C_size + (j + c_j_offset)] =
                A[(i + a_i_offset) * A_size + (j + a_j_offset)] -
                B[(i + b_i_offset) * B_size + (j + b_j_offset)];
        }
    }
}

void build_LU_from_matrix(double const * A, double * L, double * U, const int size) {
    int i, j;

    // Assignments of L and U
    for (i = 0; i < size; i++) {
        L[i * size + i] = 1;
        for (j = 0; j < i; j++) {
            L[i * size + j] = A[i * size + j];
        }
        for (j = i; j < size; j++) {
            U[i * size + j] = A[i * size + j];
        }
    }
}

void print_matrix(double const * matrix, const int size) {
    int i, j;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            printf("%f\t", matrix[i * size + j]);
        }
        printf("\n");
    }
}

int read_matrix_market(double ** matrix, char const * file) {
    int i, j;
    FILE * fp = fopen(file, "r");

    if (fp == NULL) {
        fprintf(stderr, "Specified file (%s) not valid\n", file);
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d\n", &MATRIX_SIZE);

    (*matrix) = (double *) malloc(MATRIX_SIZE * MATRIX_SIZE * sizeof(double));

    // srand(time(NULL)); // Always use the same rands for debugging purpose

    for (i = 0; i < MATRIX_SIZE; i++) {
        for (j = 0; j < MATRIX_SIZE; j++) {
            if (feof(fp)) { // Fill with random numbers if the file is not as big as specified
                (*matrix)[i * MATRIX_SIZE + j] = rand()/RAND_MAX;
            }
            fscanf(fp, "%lf", &(*matrix)[i * MATRIX_SIZE + j]);
        }
    }

    fclose(fp);

    return MATRIX_SIZE;
}

int read_matrix(double ** matrix, char const * file) {
    int i, j;
    FILE * fp = fopen(file, "r");

    fscanf(fp, "%d\n", &MATRIX_SIZE);

    (*matrix) = (double *) malloc(MATRIX_SIZE * MATRIX_SIZE * sizeof(double));

    for (i = 0; i < MATRIX_SIZE; i++) {
        for (j = 0; j < MATRIX_SIZE; j++) {
            fscanf(fp, "%lf", &(*matrix)[i * MATRIX_SIZE + j]);
        }
    }

    fclose(fp);

    return MATRIX_SIZE;
}

void LU_factorization(double * A, double * L, double * U, const int my_rank, const int world_size) {
    int K, J, I;
    int P;
    int block_size;
    int I_offset, J_offset, K_offset;

    double * aux;
    double * L_aux, * U_aux;

    block_size = MATRIX_SIZE / world_size;
    P = MATRIX_SIZE / block_size;

    aux = (double *) malloc(block_size * block_size * sizeof(double));
    L_aux = (double *) malloc(block_size * block_size * sizeof(double));
    U_aux = (double *) malloc(block_size * block_size * sizeof(double));

    for (K = 0; K < P; K++) {
        // Set memory to zero (to avoid incorrect results in the further multiplications)
        memset(L_aux, 0, block_size * block_size * sizeof(double));
        memset(U_aux, 0, block_size * block_size * sizeof(double));
        memset(aux, 0, block_size * block_size * sizeof(double));

        K_offset = K * block_size;

        // A[K][K] = L[K][K] * U[K][K]
        if ((K % world_size) == my_rank) {
            LU_factorization_block(A, L_aux, U_aux,
                MATRIX_SIZE, block_size, block_size,
                block_size,
                K_offset, K_offset,
                0, 0,
                0, 0);
        }
        diffusion(L_aux, 0, 0, block_size, block_size, (K)%world_size);
        diffusion(U_aux, 0, 0, block_size, block_size, (K)%world_size);
        diffusion(A, K_offset, K_offset, MATRIX_SIZE, block_size, (K)%world_size);

        // Once calculated LU of last block, there is no need to compute the rest of the iteration
        // This way, we avoid the calculation of the inverse of L and U, which is expensive
        if (K == P-1) {
            return;
        }

        // Compute the block column of L
        // K_offset = K * block_size;
        // Find inverse of L[K][K]
        inverse_L(L_aux, aux, block_size);
        for (J = K + 1; J < P; J++) {
            J_offset = J * block_size;
            if ((K + J) % world_size == my_rank) {
                // A = L_inv * A => L_aux = aux * A
                matrix_mult_block(aux, A, L_aux,
                    block_size,
                    block_size, MATRIX_SIZE, block_size,
                    0, 0,
                    K_offset, J_offset,
                    0, 0);
                copy_matrix_content(L_aux, block_size, 0, 0,
                    A, MATRIX_SIZE, K_offset, J_offset,
                    block_size, block_size);
            }
            diffusion(A, K_offset, J_offset, MATRIX_SIZE, block_size, (J + K) % world_size);
        }

        // Compute the block row of U
        // K_offset = K * block_size;
        // Find inverse of U[K][K]
        inverse_U(U_aux, aux, block_size);
        for (I = K + 1; I < P; I++) {
            I_offset = I * block_size;
            if ((I + K) % world_size == my_rank) {
                // A = A * U_inv => U_aux = aux * A
                matrix_mult_block(A, aux, U_aux,
                    block_size,
                    MATRIX_SIZE, block_size, block_size,
                    I_offset, K_offset,
                    0, 0,
                    0, 0);
                copy_matrix_content(U_aux, block_size, 0, 0,
                    A, MATRIX_SIZE, I_offset, K_offset,
                    block_size, block_size);
            }
            diffusion(A, I_offset, K_offset, MATRIX_SIZE, block_size, (I + K) % world_size);
        }


        // Update the trailing matrix
        // K_offset = K * block_size;
        for (J = K + 1; J < P; J++) {
            for (I = K + 1; I < P; I++) {
                if ((I + J) % world_size == my_rank || (I == J && (I % world_size) == my_rank)) {
                    J_offset = J * block_size;
                    I_offset = I * block_size;

                    matrix_mult_block(A, A, aux,
                        block_size,
                        MATRIX_SIZE, MATRIX_SIZE, block_size,
                        I_offset, K_offset,
                        K_offset, J_offset,
                        0, 0);

                    matrix_sub_block(A, aux, A,
                        block_size,
                        MATRIX_SIZE, block_size, MATRIX_SIZE,
                        I_offset, J_offset,
                        0, 0,
                        I_offset, J_offset);
                }
            }
        }
    }

    free(aux);
    free(L_aux);
    free(U_aux);
}

void forward_substitution(double * const L, double * y, double * result, const int size) {
    int i, j;

    for (j = 0; j < size; j++) {
        result[j] = y[j] / L[j * size + j];
        for (i = j+1; i < size; i++) {
            y[i] = y[i] - L[i * size + j] * result[j];
        }
    }
}

void backward_substitution(double * const U, double * y, double * result, const int size) {
    int i, j;

    for (j = size - 1; j >= 0; j--) {
        result[j] = y[j] / U[j * size + j];
        for (i = j-1; i >= 0; i--) {
            y[i] = y[i] - U[i * size + j] * result[j];
        }
    }
}
