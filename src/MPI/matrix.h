#ifndef MATRIX
#define MATRIX

int read_matrix(double ** matrix, char const * file);
int read_matrix_market(double ** matrix, char const * file);

void copy_matrix_content(
    double * src, const int size_src, const int src_i_offset, const int src_j_offset,
    double * dst, const int size_dst, const int dst_i_offset, const int dst_j_offset,
    const int num_rows, const int num_cols
);

void inverse(double * A, double * dst, const int order);
void inverse_L(double * L, double * L_inv, const int order);

void print_matrix(double const * matrix, const int size);

void LU_factorization(double * A, double * L, double * U, const int my_rank, const int world_size);

void LU_factorization_serial(double * A, double * L, double * U, const int size);

void build_LU_from_matrix(double const * A, double * L, double * U, const int size);

void matrix_mult(double const * A, double const * B, double * C);

void forward_substitution(double * const L, double * x, double * result, const int size);

void backward_substitution(double * const U, double * x, double * result, const int size);

#endif
