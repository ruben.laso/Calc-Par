#!/bin/bash

export I_MPI_DEBUG=0

number_of_nodes=(1 2 4 8 16)

echo "Nodes;Size;Time" > mpi_results.csv

for nodes in ${number_of_nodes[@]}; do
	for i in {1..5}; do
		sbatch ~/bin/mpi${nodes}.sh mpi.out -f matrix_O64.test -b x_O64.test >> mpi_results.csv
	done
done
