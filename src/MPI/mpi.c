#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <string.h>

#include "matrix.h"

#include "mpi.h"

// #define STRING_SIZE 256
static char * MATRIX_FILE;
static char * SOLUTION_FILE;

static int MATRIX_SIZE = 16;

static bool VERBOSE = false;

static const struct option long_opts [] = {
    {"matrix-file",  required_argument,  NULL,   'f'},
    {"solution-file",required_argument,  NULL,   'b'},
    {"verbose",      no_argument,        NULL,   'v'},
};

static const char short_opts [] = "f:b:v";

static void process_arguments(int argc, char * argv []);

static void read_b(char const * SOL_FILE, double * x, const int size);

int main(int argc, char *argv[]) {
    double * A;
    double * L, * U;
    double * x, * y;
    double start, end;
    int i;

    MPI_Init(&argc, &argv);
    int my_rank, world_size;

    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    process_arguments(argc, argv);

    // MATRIX_SIZE = read_matrix(&A, MATRIX_FILE);
    MATRIX_SIZE = read_matrix_market(&A, MATRIX_FILE);
    x = (double *) malloc(MATRIX_SIZE * sizeof(double));
    y = (double *) malloc(MATRIX_SIZE * sizeof(double));
    read_b(SOLUTION_FILE, x, MATRIX_SIZE);

    if (MATRIX_SIZE % world_size != 0) {
        fprintf(stderr, "MATRIX_SIZE mod world_size != 0. Exiting...\n");
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    if (VERBOSE && my_rank == 0) {
        printf("\nA=\n");
        print_matrix(A, MATRIX_SIZE);
    }

    L = (double *) calloc(MATRIX_SIZE * MATRIX_SIZE, sizeof(double));
    U = (double *) calloc(MATRIX_SIZE * MATRIX_SIZE, sizeof(double));

    if (VERBOSE && my_rank == 0) {
        printf("Computing LU factorization\n");
    }

    start = MPI_Wtime();
    LU_factorization(A, L, U, my_rank, world_size);

    build_LU_from_matrix(A, L, U, MATRIX_SIZE);

    if (VERBOSE && my_rank == 0) {
        printf("\nA=\n");
        print_matrix(A, MATRIX_SIZE);
    }

    if (VERBOSE && my_rank == 0) {
        printf("\nL =\n");
        print_matrix(L, MATRIX_SIZE);

        printf("\nU =\n");
        print_matrix(U, MATRIX_SIZE);
        printf("\n");
    }

    if (VERBOSE && my_rank == 0) {
        printf("Checking A = LU\n");
        matrix_mult(L, U, A);
        print_matrix(A, MATRIX_SIZE);
    }

    forward_substitution(L, x, y, MATRIX_SIZE);

    backward_substitution(U, y, x, MATRIX_SIZE);
    end = MPI_Wtime();

    if (VERBOSE && my_rank == 0) {
        printf("\nSolution of linear system:\n");
        for (i = 0; i < MATRIX_SIZE; i++) {
            printf("%f\t", x[i]);
        }
        printf("\n");
    }

    free(A);
    free(L);
    free(U);

    if (my_rank == 0) {
        printf("%d;%d;%f\n", world_size, MATRIX_SIZE, end-start);
    }

    MPI_Finalize();

    return EXIT_SUCCESS;
}

static void read_b(char const * SOL_FILE, double * x, const int size) {
    int i;
    FILE * fp = fopen(SOL_FILE, "r");

    if (fp == NULL) {
        fprintf(stderr, "File not found. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < size; i++) {
        fscanf(fp, "%lf", x+i);
    }
    fclose(fp);
}

static void process_arguments (int argc, char * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 'f':
                MATRIX_FILE = optarg;
                break;
            case 'b':
                SOLUTION_FILE = optarg;
                break;
            case 'v':
                VERBOSE = true;
                break;
            default:
                break;
        }
    }
}
