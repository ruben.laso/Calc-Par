#include "sort.h"

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sched.h>

static element_t elements_max_value = (element_t) RAND_MAX;
static element_t elements_min_value = (element_t) 0;
// static int number_of_threads_allowed = 24;

static void print_array (const double * array, const int size) {
    int i;

    for (i = 0; i < size; i++) {
        printf("%lf ", array[i]);
    }
    printf("\n");
}

// void set_max_threads(const int num_threads) {
//     number_of_threads_allowed = num_threads;
// }

static void merge (element_t * array, const int size) {
    element_t * read_array;
    int left_subarray_index = 0;
    int middle_position = size / 2;
    int right_subarray_index = middle_position;
    int array_index;

    read_array = (element_t *) malloc(size * sizeof(element_t));
    memcpy(read_array, array, size * sizeof(element_t));

    for (array_index = 0; array_index < size; array_index++) {
        if (left_subarray_index < middle_position &&
            (right_subarray_index >= size || read_array[left_subarray_index] < read_array[right_subarray_index]))
        {
            array[array_index] = read_array[left_subarray_index];
            left_subarray_index++;
        } else {
            array[array_index] = read_array[right_subarray_index];
            right_subarray_index++;
        }
    }

    free(read_array);
}

void merge_sort (element_t * array, const int size) {
    if (size < 2) {
        return;
    }

    int middle_position = size / 2;

    merge_sort(array, middle_position);
    merge_sort(array + middle_position, size - middle_position);

    merge(array, size);
}


inline static void swap (element_t * a, element_t * b) {
    element_t aux = *a;
    *a = *b;
    *b = aux;
}

static int partition(element_t * array, const int left, int right) {
    element_t pivot, t;
    int i, j;
    pivot = array[left];
    i = left; j = right+1;

    while(1) {
        do {
            ++i;
        } while( array[i] <= pivot && i <= right );
        do {
            --j;
        } while( array[j] > pivot );

       	if( i >= j ) {
            break;
        }
       	t = array[i]; array[i] = array[j]; array[j] = t;
        // swap(array+left, array+j);
    }
    t = array[left]; array[left] = array[j]; array[j] = t;
    // swap(array+left, array+j);
    return j;
}


void quick_sort (element_t * array, const int first, const int last) {
    int partition_index, tid;

    if (first < last) {
        partition_index = partition(array, first, last);

        quick_sort(array, first, partition_index-1);
        quick_sort(array, partition_index+1, last);
    }
}


static void divide_buckets (
    const element_t * const array,
    const int array_size,
    element_t *** subbuckets,
    const int number_of_buckets,
    const int size_of_buckets,
    int ** subbuckets_indexes)
{
    int i;
    int selected_bucket;
    int value_interval = ceil( (elements_max_value - elements_min_value) / (float) number_of_buckets);
    int tid;
    element_t element;

    int first, last;
    int chunk = array_size / number_of_buckets;

    #pragma omp parallel num_threads(number_of_buckets) default(none) \
    private(i, element, selected_bucket, tid, first, last) \
    firstprivate(array, subbuckets, subbuckets_indexes, chunk, array_size, value_interval, elements_min_value)
    {
        tid = omp_get_thread_num();
        first = tid * chunk;
        last = (tid == (number_of_buckets - 1)) ? array_size : first + chunk;

        for (i = first; i < last; i++) {
            element = array[i];
            selected_bucket = (element - elements_min_value) / value_interval;
            subbuckets[tid][selected_bucket][subbuckets_indexes[tid][selected_bucket]] = element;
            subbuckets_indexes[tid][selected_bucket]++;
        }
    }
}

static void gather_subbuckets (
    element_t ** buckets,
    element_t *** subbuckets,
    int * buckets_indexes,
    int ** subbuckets_indexes,
    const int number_of_buckets
) {
    int tid;
    int i, j;
    element_t element;
    int position;

    #pragma omp parallel num_threads(number_of_buckets) private(i, j, element, tid, position) firstprivate(buckets, subbuckets)
    {
        tid = omp_get_thread_num();
        position = 0;

        for (i = 0; i < number_of_buckets; i++) {
            for (j = 0; j < subbuckets_indexes[i][tid]; j++) {
                element = subbuckets[i][tid][j];
                buckets[tid][position] = element;
                position++;
            }
        }
        buckets_indexes[tid] = position;
    }
}


void bucket_sort(element_t * array, const int size, const int number_of_buckets,  const element_t min_element, const element_t max_element) {
    element_t ** buckets;
    element_t *** subbuckets;
    int * buckets_indexes;
    int ** subbuckets_indexes;
    int * displacements;
    int i, j;
    int size_of_buckets;
    int thread_id;

    elements_max_value = max_element;
    elements_min_value = min_element;

    // assign enough space for the buckets
    size_of_buckets = size / ((number_of_buckets - 1) <= 0 ? 1 : (number_of_buckets - 1));
    size_of_buckets += size / 4;

    buckets_indexes = (int *) malloc(number_of_buckets * sizeof(int));
    displacements = (int *) malloc(number_of_buckets * sizeof(int));

    subbuckets_indexes = (int **) malloc(number_of_buckets * sizeof(int *));
    buckets = (double **) malloc(number_of_buckets * sizeof(double *));
    subbuckets = (double ***) malloc(number_of_buckets * sizeof(double **));
    for (i = 0; i < number_of_buckets; i++) {
        buckets_indexes[i] = 0;
        subbuckets_indexes[i] = (int *) malloc(number_of_buckets * sizeof(int));
        buckets[i] = (double *) malloc(size_of_buckets * sizeof(double *));
        subbuckets[i] = (double **) malloc(number_of_buckets * sizeof(double *));
    }

    for (i = 0; i < number_of_buckets; i++) {
        for (j = 0; j < number_of_buckets; j++) {
            subbuckets_indexes[i][j] = 0;
            subbuckets[i][j] = (double *) malloc((size_of_buckets / number_of_buckets) * sizeof(double));
        }
    }

    divide_buckets(array, size, subbuckets, number_of_buckets, size_of_buckets, subbuckets_indexes);

    gather_subbuckets(buckets, subbuckets, buckets_indexes, subbuckets_indexes, number_of_buckets);

    for (i = 0, j = 0; i < number_of_buckets; i++) {
        displacements[i] = j;
        j += buckets_indexes[i];
    }

    #pragma omp parallel num_threads(number_of_buckets) private(thread_id) firstprivate(buckets, buckets_indexes, array, displacements)
    {
        thread_id = omp_get_thread_num();
        // Sort the bucket
        merge_sort(buckets[thread_id], buckets_indexes[thread_id]);
        memcpy(array + displacements[thread_id], buckets[thread_id], buckets_indexes[thread_id] * sizeof(double));
    }

    for (i = 0; i < number_of_buckets; i++) {
        free(buckets[i]);
        for (j = 0; j < number_of_buckets; j++) {
            free(subbuckets[i][j]);
        }
        free(subbuckets[i]);
        free(subbuckets_indexes[i]);
    }
    free(buckets);
    free(subbuckets);
    free(subbuckets_indexes);
    free(buckets_indexes);
    free(displacements);
}
