#ifndef SORT
#define SORT

typedef double element_t;

void bucket_sort (element_t * array, const int size, const int number_of_buckets, const element_t min_element, const element_t max_element);

void merge_sort (element_t * array, const int size);

// void set_max_threads(const int num_threads);

#endif
