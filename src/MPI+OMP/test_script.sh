program=mpi_openmp.out
size=100000000
threads=(1 2 4 8 16)

export I_MPI_DEBUG=0

for thread in ${threads[@]}; do
	for i in {1..5}; do
		sbatch ~/bin/mpi${thread}x${thread}.sh ${program} -s ${size} -t ${thread}
	done;
done;
