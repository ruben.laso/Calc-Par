#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <getopt.h>
#include <sys/time.h>
#include <time.h>
#include <float.h>
#include <string.h>

#include "sort.h"

static int size_of_problem = 1000;
static long long elements_max_value = RAND_MAX;
static bool debug_mode = false;
static int number_of_buckets = 3;
static int number_of_threads = 3;

static const struct option long_opts [] = {
    {"size",        required_argument,  NULL,   's'},
    {"threads",     required_argument,  NULL,   't'},
    {"debug",       no_argument,        NULL,   'v'},
    {"limit",       no_argument,        NULL,   'l'},
};

static const char short_opts [] = "s:t:vl:";

static void process_arguments (int argc, char * argv []);

inline static void print_array (const double * array, const int size);

static void divide_buckets (
    const double * array,
    const int array_size,
    double * buckets,
    const int number_of_buckets,
    const int size_of_buckets,
    int * buckets_indexes)
{
    int i;
    int selected_bucket;
    double element;
    int value_interval = ceil(elements_max_value / (float) number_of_buckets);

    for (i = 0; i < array_size; i++) {
        element = array[i];
        selected_bucket = element / value_interval;
        buckets[selected_bucket * size_of_buckets + buckets_indexes[selected_bucket]++] = element;
    }
}

static void gather_subbuckets (
    const double * subbuckets,
    double * buckets,
    const int * subbuckets_indexes,
    int * buckets_indexes,
    const int * array_chunks,
    const int size_of_subbucket,
    const int size_of_buckets
) {
    int i, j, k;
    int bucket_length;
    int * aux_displacements;
    int * aux;
    int rank;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    aux_displacements = (int *) malloc(number_of_buckets * sizeof(int));
    aux = (int *) malloc(number_of_buckets * sizeof(int));

    // for each bucket, gather all subbuckets.
    for (i = 0; i < number_of_buckets; i++) {
        MPI_Gather(subbuckets_indexes + i, 1, MPI_INT, aux, 1, MPI_INT, 0, MPI_COMM_WORLD);

        if (rank == 0) {
            for (j = 0, bucket_length = 0; j < number_of_buckets; j++) {
                bucket_length += aux[j];
            }
            buckets_indexes[i] = bucket_length;
            for (j = 0, k = 0; j < number_of_buckets; j++) {
                aux_displacements[j] = k;
                k += aux[j];
            }
        }

        MPI_Gatherv(
            subbuckets + (i * size_of_subbucket), subbuckets_indexes[i], MPI_DOUBLE,
            buckets + (i * size_of_buckets), aux,
            aux_displacements, MPI_DOUBLE,
            0, MPI_COMM_WORLD
        );
    }

    free(aux_displacements);
    free(aux);
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    int rank;
    int array_chunk;
    int last_bucket_chunk;
    MPI_Request request;
    double * buckets;
    double * subbuckets;
    double * private_bucket;
    int * displacements;
    int * buckets_indexes;
    int * subbuckets_indexes;
    int * array_chunks;
    double * array;
    int i;
    int size_of_buckets;
    double bucket_element_max;

    double time_init, time_end;

    MPI_Comm_size(MPI_COMM_WORLD, &number_of_buckets);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    process_arguments(argc, argv);
    // set_max_threads(number_of_threads);

    if (debug_mode) {
        printf("Arguments parsed\n");
    }
    bucket_element_max = elements_max_value / (double) number_of_buckets;

    array = (double *) malloc(size_of_problem * sizeof(double));

    if (debug_mode && rank == 0) {
        for (i = 0; i < size_of_problem; i++) {
            array[i] = (double) (rand() % elements_max_value);
        }
    } else if (rank == 0) {
        srand(time(NULL));
        for (i = 0; i < size_of_problem; i++) {
            array[i] = (double) rand();
        }
    }

    size_of_buckets = size_of_problem / ((number_of_buckets - 1) == 0 ? 1 : (number_of_buckets - 1));
    array_chunk = size_of_problem / (float)number_of_buckets;
    last_bucket_chunk = size_of_problem - (array_chunk * (number_of_buckets - 1));

    buckets = (double *) malloc(number_of_buckets * size_of_buckets * sizeof(double));
    subbuckets = (double *) malloc(number_of_buckets * array_chunk * sizeof(double));
    private_bucket = (double *) malloc(size_of_buckets * sizeof(double));
    buckets_indexes = (int *) malloc(number_of_buckets * sizeof(int));
    displacements = (int *) malloc(number_of_buckets * sizeof(int));
    subbuckets_indexes = (int *) malloc(number_of_buckets * sizeof(int));
    array_chunks = (int *) malloc(number_of_buckets * sizeof(int));

    for (i = 0; i < number_of_buckets; i++) {
        buckets_indexes[i] = 0;
        subbuckets_indexes[i] = 0;
        displacements[i] = i * array_chunk;
        array_chunks[i] = array_chunk;
    }
    array_chunks[i - 1] = last_bucket_chunk;

    if (debug_mode && rank == 0) {
        printf("Initial array:\n");
        print_array(array, size_of_problem);
        printf("\n");
    }

    time_init = MPI_Wtime();

    // Send to each process its chunk of the array
    MPI_Scatterv(array, array_chunks, displacements, MPI_DOUBLE, private_bucket, array_chunks[rank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Each process divide its chunk of the array
    divide_buckets(private_bucket, array_chunks[rank], subbuckets, number_of_buckets, array_chunk, subbuckets_indexes);

    // Gather process subbuckets to one general bucket
    gather_subbuckets(subbuckets, buckets, subbuckets_indexes, buckets_indexes, array_chunks, array_chunk, size_of_buckets);

    if (rank == 0) {
        for (i = 0; i < number_of_buckets; i++) {
            displacements[i] = i * size_of_buckets;
        }
    }

    // Send indexes to all processes (non-blocking). Necessary for correct array sort.
    MPI_Ibcast(buckets_indexes, number_of_buckets, MPI_INT, 0, MPI_COMM_WORLD, &request);

    // Send the buckets to the processes
    MPI_Scatterv(buckets, buckets_indexes, displacements, MPI_DOUBLE, private_bucket, size_of_buckets, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Wait for broadcast to be completed
    MPI_Wait(&request, MPI_STATUS_IGNORE);

    // Sort the bucket
    bucket_sort(private_bucket, buckets_indexes[rank], number_of_threads, rank * bucket_element_max, (rank+1) * bucket_element_max);

    displacements[0] = 0;
    for (i = 1; i < number_of_buckets; i++) {
        displacements[i] = displacements[i-1] + buckets_indexes[i-1];
    }

    // Wait for all processes to complete the sort
    MPI_Barrier(MPI_COMM_WORLD);

    // Gather all buckets and build the ordered array
    MPI_Gatherv(private_bucket, buckets_indexes[rank], MPI_DOUBLE, array, buckets_indexes, displacements, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    time_end = MPI_Wtime();

    if (debug_mode && rank == 0) {
        printf("\nPrinting final results:\n");
        print_array(array, size_of_problem);
        printf("\n\n");
    }

    if (rank == 0) {
        printf("%d;%d;%f\n", number_of_buckets, number_of_threads, time_end - time_init);
    }

    free(private_bucket);
    free(buckets);
    free(buckets_indexes);
    free(displacements);
    free(array);

    MPI_Finalize();

    return EXIT_SUCCESS;
}

inline static void print_array (const double * array, const int size) {
    int i;

    for (i = 0; i < size; i++) {
        printf("%lf ", array[i]);
    }
    printf("\n");
}

static void process_arguments (int argc, char * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 's':
                size_of_problem = atoi(optarg);
                break;
            case 'v':
                debug_mode = true;
                break;
            case 'l':
                elements_max_value = atoi(optarg);
                break;
            case 't':
                number_of_threads = atoi(optarg);
                break;
            default:
                break;
        }
    }
}
