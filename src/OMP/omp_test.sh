#!/bin/bash

program="./openmp.out"
repetitions=10
sizes=(10000)
num_threads=(1 2 4 8 16)

for threads in ${num_threads[@]}; do
    for size in ${sizes[@]}; do
        for (( i = 0; i < ${repetitions}; i++ )); do
            ${program} -n ${threads} -h ${size} -v ${size}
        done
    done
done
