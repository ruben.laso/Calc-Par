#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>
#include <math.h>

#include <sys/time.h>

#include <omp.h>

static int SIZE_HOR = 10;
static int SIZE_VER = 10;
static int INNER_HOR_MIN = 1;
static int INNER_VER_MIN = 1;
static int INNER_HOR_MAX = 9;
static int INNER_VER_MAX = 9;

static double BC_LEFT = 100;
static double BC_RIGHT = 100;
static double BC_LOWER = 100;
static double BC_UPPER = 0;

static double TOLERANCE = 1e-3;
static int MAX_ITER = 100;

static bool VERBOSE = false;
static bool PRINT_RESULTS = false;

static int NUM_THREADS = 1;

static const struct option long_opts [] = {
    {"horizontal",   required_argument,  NULL,   'h'},
    {"vertical",     required_argument,  NULL,   'v'},
    {"tolerance",    required_argument,  NULL,   't'},
    {"max-iter",     required_argument,  NULL,   'i'},
    {"num-threads",  required_argument,  NULL,   'n'},
    {"verbose",      no_argument,        NULL,   'd'},
    {"results",      no_argument,        NULL,   'r'},
};

static const char short_opts [] = "h:v:t:i:n:dr";

static void process_arguments (int argc, char const * argv []);

static bool tolerance_reached (double const * last_iter_mesh, double const * mesh, const double tolerance);

static void print_mesh (double const * mesh);

int main(int argc, char const *argv[]) {
    int iter, i, j;
    double * mesh = NULL;
    double * last_iter_mesh = NULL;
    struct timeval start, end;
    double time_sec;

    process_arguments(argc, argv);

    if (VERBOSE) {
        printf("Verbose enabled\n");
        printf("Mesh size: %d x %d\n", SIZE_HOR, SIZE_VER);
        printf("Tolerance: %f\n", TOLERANCE);
        printf("Max. iterations: %d\n", MAX_ITER);
        printf("Number of threads: %d\n", NUM_THREADS);
        printf("Boundary conditions (L,R,U,B): %f,%f,%f,%f\n", BC_LEFT, BC_RIGHT, BC_UPPER, BC_LOWER);
    }

    mesh = (double *) calloc(SIZE_HOR * SIZE_VER, sizeof(double));
    last_iter_mesh = (double *) calloc(SIZE_HOR * SIZE_VER, sizeof(double));

    // if (VERBOSE) {
    //     printf("Initial mesh:\n");
    //     print_mesh(mesh);
    // }

    // Start of the algorithm
    gettimeofday(&start, NULL);

    // INNER_HOR_MIN = 1;
    // INNER_VER_MIN = 1;
    INNER_HOR_MAX = SIZE_HOR == 1? 1 : SIZE_HOR - 1;
    INNER_VER_MAX = SIZE_VER == 1? 1 : SIZE_VER - 1;

    // Set Boundary conditions
    // Top and bottom conditions
    #pragma omp parallel for num_threads(NUM_THREADS) \
        private(i) \
        firstprivate(BC_LOWER, BC_UPPER, BC_LEFT, BC_RIGHT, mesh, last_iter_mesh) \
        schedule(static, 8)
    for (i = 0; i < SIZE_HOR; i++) {
        mesh[i] = BC_LOWER;
        last_iter_mesh[i] = BC_LOWER;
        mesh[(SIZE_VER-1) * SIZE_HOR + i] = BC_UPPER;
        last_iter_mesh[(SIZE_VER-1) * SIZE_HOR + i] = BC_UPPER;
    }

    // Left and right conditions
    #pragma omp parallel for num_threads(NUM_THREADS) \
        private(i) \
        firstprivate(BC_LOWER, BC_UPPER, BC_LEFT, BC_RIGHT, mesh, last_iter_mesh) \
        schedule(static)
    for (i = 1; i < (SIZE_VER - 1); i++) {
        mesh[i * SIZE_HOR] = BC_LEFT;
        last_iter_mesh[i * SIZE_HOR] = BC_LEFT;
        mesh[i * SIZE_HOR + SIZE_HOR - 1] = BC_RIGHT;
        last_iter_mesh[i * SIZE_HOR + SIZE_HOR - 1] = BC_RIGHT;
    }

    if (VERBOSE) {
        printf("Computing mesh values\n");
    }

    // Calculate the inner tiles
    iter = 0;
    do {
        // black tiles
        #pragma omp parallel for num_threads(NUM_THREADS) \
            private(i, j) \
            firstprivate(last_iter_mesh, mesh) \
            schedule(static, 4)
        for (i = INNER_VER_MIN; i < INNER_VER_MAX; i++) {
            for (j = (i%2==0)? INNER_HOR_MIN+1: INNER_HOR_MIN; j < INNER_HOR_MAX; j+=2) {
                last_iter_mesh[i * SIZE_HOR + j] = mesh[i * SIZE_HOR + j];

                mesh[i * SIZE_HOR + j] = (mesh[i * SIZE_HOR + (j-1)] +
                                          mesh[i * SIZE_HOR + (j+1)] +
                                          mesh[(i-1) * SIZE_HOR + j] +
                                          mesh[(i+1) * SIZE_HOR + j]) / 4;
            }
        }

        // red tiles
        #pragma omp parallel for num_threads(NUM_THREADS) \
            private(i, j) \
            firstprivate(last_iter_mesh, mesh) \
            schedule(static, 4)
        for (i = INNER_VER_MIN; i < INNER_VER_MAX; i++) {
            for (j = (i%2==0)? INNER_HOR_MIN: INNER_HOR_MIN+1; j < INNER_HOR_MAX; j+=2) {
                last_iter_mesh[i * SIZE_HOR + j] = mesh[i * SIZE_HOR + j];

                mesh[i * SIZE_HOR + j] = (mesh[i * SIZE_HOR + (j-1)] +
                                          mesh[i * SIZE_HOR + (j+1)] +
                                          mesh[(i-1) * SIZE_HOR + j] +
                                          mesh[(i+1) * SIZE_HOR + j]) / 4;
            }
        }
        iter++;
    } while (iter < MAX_ITER && !tolerance_reached(last_iter_mesh, mesh, TOLERANCE));

    // End of the algorithm
    gettimeofday(&end, NULL);

    time_sec = (end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1.e6);

    if (PRINT_RESULTS) {
        print_mesh(mesh);
    }

    printf("%d;%d;%d;%d;%f\n", NUM_THREADS, iter, SIZE_HOR, SIZE_VER, time_sec);

    free(mesh);
    free(last_iter_mesh);

    return EXIT_SUCCESS;
}

static void print_mesh (double const * mesh) {
    int i, j;
    for (i = SIZE_HOR - 1; i >= 0; i--) {
        for (j = 0; j < SIZE_VER; j++) {
            printf("%10.3lf    ", mesh[i * SIZE_HOR + j]);
        }
        printf("\n");
    }
}

static bool tolerance_reached (double const * last_iter_mesh, double const * mesh, const double tolerance) {
    int i, j;

    for (i = 0; i < SIZE_HOR; i++) {
        for (j = 0; j < SIZE_VER; j++) {
            if (abs(last_iter_mesh[i * SIZE_HOR + j] - mesh[i * SIZE_HOR + j]) > tolerance) {
                return false;
            }
        }
    }

    return true;
}

static void process_arguments (int argc, char const * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 'h':
                SIZE_HOR = atoi(optarg);
                break;
            case 'v':
                SIZE_VER = atoi(optarg);
                break;
            case 't':
                TOLERANCE = atof(optarg);
                break;
            case 'i':
                MAX_ITER = atoi(optarg);
                break;
            case 'n':
                NUM_THREADS = atoi(optarg);
                break;
            case 'd':
                VERBOSE = true;
                break;
            case 'r':
                PRINT_RESULTS = true;
                break;
            default:
                break;
        }
    }
}
