\section{OpenMP} \label{sec:openmp}
Para el programa utilizando OpenMP se ha resuelto un problema de calor en una placa rectangular. Este problema viene definido tal que:
\begin{gather*}
	\frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} = 0
	\\
	T(x,0) = 100, \quad T(0,y) = 100, \quad T(a,y) = 100, \quad T(x,b) = 0;
\end{gather*}

De forma que el esquema en diferencias finitas sería
\begin{equation}
	T_{i,j} = \frac{T_{i,j-1} + T_{i,j+1} + T_{i-1,j} + T_{i+1,j}}{4}
\end{equation}

La idea del algoritmo ``Rojo-Negro'' es reducir las dependencias de datos (pues cada uno de los nodos depende de sus cuatro contiguos) al establecer una división similar a la que se puede ver en un tablero de damas o ajedrez (ver figura~\ref{fig:checker}), de forma que primero actualizaremos los nodos correspondientes a un color, para después actualizar los valores del otro color.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\linewidth]{figuras/checker}
\caption{Representación gráfica del método ``Rojo-Negro''.}
\label{fig:checker}
\end{figure}

Se ha de tener en cuenta que los nodos de los laterales se ven afectados por las condiciones de contorno impuestas, de forma que no deben ser actualizados.

Para la ejecución de este método se deben imponer dos condiciones de finalización, bien por número máximo de iteraciones o porque la diferencia de temperatura entre dos instantes de tiempo consecutivos es lo suficientemente pequeña como para considerar que se ha alcanzado un estado estacionario.

De esta forma, se ha llegado al código~\ref{lst:red-black-openmp} (aquí se muestra solamente el núcleo del programa, se puede ver completamente en los ficheros adjuntos a este documento) donde debemos destacar la presencia de dos bloques de lazos, cada uno asociado a un color y el alto paralelismo que podemos aplicar en estos, debido a que hemos anulado la dependencia respecto de las celdas contiguas.

\lstinputlisting[caption={Código relativo al esquema de diferencias finitas ``Rojo-Negro''.}, label={lst:red-black-openmp}, firstline=112, lastline=149]{../src/OMP/openmp.c}

También se deben mencionar dos puntos respecto a las directivas OMP utilizadas:
\begin{itemize}
	\item El uso de \texttt{num\_threads(NUM\_THREADS)} para especificar mediante un argumento de nuestro programa con cuántos hilos queremos que se ejecute la región paralela\footnote{también podríamos utilizar la variable de entorno \texttt{OMP\_NUM\_THREADS} o la función \texttt{omp\_set\_num\_threads()}}.
	
	\item El uso de un \emph{schedule} estático debido a que la cantidad de trabajo se presupone igual para todos los hilos. El tamaño mínimo del \emph{chunk} de cada hilo se determina a 4, debido a que va ser el número de elementos en una línea caché sobre los que va a trabajar\footnote{En cada línea caché caben 8 \emph{doubles}, y cada hilo trabajará sobre 4 alternos, es decir, trabajará sobre los elementos primero, tercero, quinto y séptimo, por ejemplo.} previniendo así problemas de \emph{false sharing} en el código.
	
	\item La privacidad de los punteros puede ser \texttt{firstprivate}, dado que ningún hilo va a sobreescribir el valor del puntero (aunque sí los valores contenidos en las direcciones apuntadas) al mismo tiempo que se debe mantener su valor inicial. El que no existan variables compartidas entre hilos hace que evitemos tener que imponer medidas contra las carreras críticas y la falsa compartición.
\end{itemize}

Para la inicialización de la malla respecto a las condiciones de contorno, estas se muestran en el código~\ref{lst:red-black-ic}.
\lstinputlisting[caption={Inicialización de las condiciones de contorno del problema.}, label={lst:red-black-ic}, firstline=83, lastline=106]{../src/OMP/openmp.c}

Aquí debemos destacar:
\begin{itemize}
	\item El uso de la directiva \texttt{firstprivate()} para denotar el grado de ``privacidad'' que debe tener cada variable dentro de la región paralela, que en este caso deben ser privadas (aún conservando el valor que tenían antes de entrar en la región).
	
	\item La especificación de un \textit{schedule} estático con diferentes \textit{chunks} para evitar problemas de falsa compartición.
\end{itemize}

Por último, debemos comentar las opciones de ejecución del programa:
\begin{compactitem}
	\item \texttt{-h} define el tamaño horizontal de la malla.
	\item \texttt{-v} define el tamaño vertical de la malla.
	\item \texttt{-t} define la tolerancia de la condición de parada del método.
	\item \texttt{-i} define el número máximo de iteraciones del método.
	\item \texttt{-n} define el número de \emph{threads} con los que se ejecutará el programa.
	\item \texttt{-d} activa el modo \emph{debug}, donde aparecerán mensajes informativos.
	\item \texttt{-r} imprime la malla al finalizar la ejecución del programa.
\end{compactitem}

El resto de características menos relevantes del código se encuentran debidamente comentadas dentro del fichero fuente.

Este programa se ha ejecutado utilizando una malla de $10,000\times10,000$ elementos, utilizando 1, 2, 4, 8 y 16 hilos. Los resultados de dicha ejecución se muestran en la tabla~\ref{tab:openmp-speedup} (ver tiempos completos en la tabla~\ref{tab:openmp} del apéndice~\ref{ap:openmp}) y la figura~\ref{fig:openmp}.

\input{tablas/openmp-speedup}
\input{graficas/openmp}

Con este programa podemos ver un \textit{speedup} bastante bueno, pues obtenemos una eficiencia superior al 90\% cuando llegamos a los 4 hilos y del 74\% cuando lo ejecutamos con 8. Se debe comentar que el mal dato obtenido al ejecutarlo con 16 \textit{threads} se debe en gran parte a que el procesador sólo dispone de 12 \textit{cores} físicos, es decir, entramos en la barrera del \textit{hyperthreading}, y por tanto comenzamos a tener varios hilos por núcleo (donde el rendimiento se resiente).

También debemos comentar que es lógico que llegue un punto en que al añadir más hilos de ejecución a nuestro programa no obtengamos ganancia, pues las partes secuenciales que podamos tener en nuestro código cobrarán más peso, además de que el coste de crear hilos es muy elevado.