\section{MPI y OpenMP} \label{sec:MPI+OMP}
Por último, se ha implementado un programa que incorpore ambas tecnologías de paralelismo. Para ello, se ha escogido el algoritmo de ordenación \emph{bucket sort}, que a pesar de no ser óptimo en su versión secuencial, sí puede proporcionar muy buenos resultados una vez paralelizado.

Un ejemplo visual de cómo funciona el \emph{bucket sort} se puede ver en la figura~\ref{fig:bucketsort}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.265\linewidth]{figuras/bucketsort}
	\caption{Ejemplo de funcionamiento del algoritmo \emph{bucket sort}.}
	\label{fig:bucketsort}
\end{figure}

La idea de este algoritmo es, dado un \textit{array} de elementos, dividir éstos en pequeños grupos (\emph{buckets}) según su valor numérico. Una vez realizada la división en \emph{buckets}, cada uno de éstos se ordena utilizando un algoritmo de ordenación clásico como \emph{merge sort} o \emph{quick sort}. Finalmente, se juntan todos los \emph{buckets} en el \emph{array} final ya ordenado. 

Así pues, el código asociado a la parte paralelizada con MPI se muestra en el código~\ref{lst:MPI-bucket}, mientras que las funciones \texttt{divide\_buckets()} y \texttt{gather\_subbuckets()} se muestran en los códigos~\ref{lst:MPI-divide}~y~\ref{lst:MPI-gather} respectivamente.


\lstinputlisting[language=C, caption={Implementación del algoritmo \emph{bucket sort} con MPI}, label={lst:MPI-bucket}, firstline=172, lastline=208]{../src/MPI+OMP/mpi_openmp.c}

\lstinputlisting[language=C, caption={Código de la función \texttt{divide\_buckets()}.}, label={lst:MPI-divide}, firstline=33, lastline=51]{../src/MPI+OMP/mpi_openmp.c}

\lstinputlisting[language=C, caption={Código de la función \texttt{gather\_subbuckets()}.}, label={lst:MPI-gather}, firstline=53, lastline=98]{../src/MPI+OMP/mpi_openmp.c}

Es importante mencionar que la ordenación de cada uno de los \emph{buckets} MPI también se realiza mediante \emph{bucket sort}, salvo que en este último caso se paraleliza con OpenMP. Como la estructura principal del código es muy similar a la de MPI, se comentan sólo las partes paralelas del mismo.

En primer lugar, en el código~\ref{lst:OMP-divide} se muestra la función \texttt{divide\_buckets()}, donde cada uno de los hilos lee una sección del \emph{array} y divide sus elementos en una colección de \emph{buckets} privados. Podría pensarse que esto podría hacerse con una directiva \texttt{\#pragma omp parallel for}, sin embargo, eso conllevaría hacer múltiples llamadas a \texttt{omp\_get\_thread\_num()} dentro del bucle para que cada hilo se identificase y escribiese en su colección privada de \emph{buckets}. De esta forma, sólo se realiza una llamada a esta función y cada hilo sólo clasifica una parte del \emph{array}, maximizando la eficiencia. Así pues, si tenemos $n$ hilos, tendremos $n$ grupos de $n$ \emph{buckets} cada uno.

\lstinputlisting[language=C, caption={Código de la función \texttt{divide\_buckets()} paralelizado con OpenMP.}, label={lst:OMP-divide}, firstline=111, lastline=143]{../src/MPI+OMP/sort.c}

Para la operación \texttt{gather\_subbuckets()} (ver código~\ref{lst:OMP-gather}) se realiza algo similar a lo comentado anteriormente, donde cada hilo se va a encargar de reunir en un único \emph{bucket} todos los \emph{subbuckets} generados anteriormente. Al final de este paso, tendremos $n$ \emph{buckets} que ya pueden ser ordenados.

\lstinputlisting[language=C, caption={Código de la función \texttt{gather\_subbuckets()} paralelizado con OpenMP.}, label={lst:OMP-gather}, firstline=145, lastline=172]{../src/MPI+OMP/sort.c}

Para la ordenación de cada uno de estos \emph{buckets} se utiliza un algoritmo \emph{merge sort}~\cite{merge}. Como se puede ver en el código~\ref{lst:OMP-merge}, cada hilo se ocupará de ordenar un \emph{bucket} para posteriormente copiar su contenido en las correspondientes posiciones del \emph{array} resultado.

\lstinputlisting[language=C, caption={Código para la ordenación de \emph{buckets} y copia en el \emph{array} principal paralelizado con OpenMP.}, label={lst:OMP-merge}, firstline=220, lastline=227]{../src/MPI+OMP/sort.c}

Así pues, los resultados se muestran en la tabla~\ref{tab:MPI-OMP-speedup} y la figura~\ref{fig:MPI-OMP-speedup} (también se pueden consultar todos los tiempos de ejecución en la tabla~\ref{tab:MPI-OMP} del apéndice~\ref{ap:MPI-OMP}).

\input{tablas/MPI-OMP-speedup}

\input{graficas/MPI-OMP-speedup}

Como se puede comprobar en los datos obtenidos, este programa escala razonablemente bien hasta la combinación de 4 procesos con 4 hilos por proceso y, a partir de ahí, la eficiencia baja razonablemente. Esto es debido a que el número total de hilos trabajando es muy alto y el coste de levantar tantos hilos, sincronizarlos, y la comunicación entre procesos aumenta de forma considerable. Aunque el número de elementos a ordenar por cada proceso/hilo disminuye, esto no es suficiente para enmascarar todo el coste de paralelizar el algoritmo con tantos procesos e hilos. Además, se debe comentar que al utilizar 16 hilos, comenzamos a utilizar la tecnología \emph{hyperthreading} del procesador, por lo que el rendimiento ya no escalará tan bien, como ya se comentó en la sección~\ref{sec:openmp}.

Así pues, debemos destacar que el pasar de un único proceso con un único hilo a dos procesos con dos hilos por proceso, disminuye el tiempo de unos 43 segundos a 13, lo cual implica un \textit{speedup} de $3.51$, un dato realmente bueno. Incluso al pasar a una configuración de 4 procesos y 4 hilos mantenemos una eficiencia de 73\% que es más que satisfactoria. A partir de ahí, los tiempos de ejecución son tan bajos que la sobrecarga respecto a levantar hilos, procesos y comunicaciones mencionada anteriormente es realmente difícil de enmascarar.