\section{MPI} \label{sec:mpi}
Para el programa que utiliza MPI para la paralelización se ha implementado la factorización LU por bloques. El pseudo-código de este algoritmo se muestra en el algoritmo~\ref{alg:LU}.

Aquí es importante comentar que las funciones \emph{diffusion\_by\_row()} y \emph{diffusion\_by\_column()} pueden ser implementadas utilizando \texttt{MPI\_Bcast()}, pues finalmente todos los procesos van a necesitar la información compartida en esas funciones.

En este algoritmo, primero se divide la matriz $A$ en $P$ bloques. Entonces, en cada iteración del lazo más externo, se calcula la factorización LU tradicional y se difunden las nuevas submatrices $L$ y $U$ a todos los nodos. Posteriormente, se actualiza la fila $K$-ésima con la operación $A_{KJ} = L_{KK}^{-1} \times A_{KJ}$, donde cada nodo sólo realiza los cálculos de los bloques que le atañen. Lo mismo se realizaría con la $K$-ésima fila con la operación $A_{IK} =  A_{IK} \times U_{KK}^{-1}$. Por último, se actualizan el resto de bloques de la matriz con $A_{IJ} = A_{IJ} - A_{IK} \times A_{KJ}$.

Se debe mencionar que las inversas de $L$ y $U$ pueden ser calculadas de dos formas diferentes. Por un lado, podemos utilizar el método de los menores principales, como se haría para cualquier otra matriz, aunque este es un método muy costoso computacionalmente. Por otro lado, y gracias a que son matrices triangulares, se puede utilizar un método de descenso/remonte para resolver los sistemas $Lx = E_n$ y $Ux = E_n$ donde $E_n$ es el vector compuesto por todo ceros, salvo la posición $n$, donde contiene un 1.

\input{codigo/LU}

Una vez implementado en el lenguaje C, se obtiene la función que se muestra en el código~\ref{lst:LU-block} (todo el resto de funciones auxiliares dentro de éste, se pueden consultar en el código fuente adjunto a este documento).

\lstinputlisting[language=C, caption={Factorización LU por bloques utilizando C y MPI.}, label={lst:LU-block}, firstline=362, lastline=477]{../src/MPI/matrix.c}

De estas funciones auxiliares, la más interesante desde el punto del uso de MPI es la función \texttt{diffusion()}, la cual se muestra en el código~\ref{lst:diffusion}.

\lstinputlisting[language=C, caption={Factorización LU por bloques utilizando C y MPI.}, label={lst:diffusion}, firstline=137, lastline=150]{../src/MPI/matrix.c}

Aquí vemos que para difundir un bloque de matriz de tamaño $n\times n$ es necesario realizar $n$ llamadas a \texttt{MPI\_Bcast()} de $n$ elementos cada una. Esto se debe a que los datos de diferentes filas no van a estar contiguos en la memoria, y tampoco van a tener un \textit{offset} fijo, por lo que se han de realizar estas $n$ llamadas a la función.

Las opciones de ejecución de este programa son las siguientes:
\begin{compactitem}
	\item \texttt{-f} especifica el fichero desde el que se debe leer la matriz $A$.
	\item \texttt{-b} especifica el fichero desde el que se debe leer el vector $b$.
	\item \texttt{-v} activa el modo \textit{verbose}, donde aparecerán mensajes informativos durante la ejecución.
\end{compactitem}

Los resultados de ejecución\footnote{Se ha de comentar que en el tiempo de ejecución solo se incluye la factorización y la resolución del sistema. La lectura de datos de ficheros quedan fuera de esta medida.} del programa que utiliza la factorización LU por bloques en sistemas de tamaño 1024 se muestran tanto en la tabla~\ref{tab:MPI} (ver tiempos completos en la tabla~\ref{tab:MPI} del apéndice~\ref{ap:MPI}) como en la figura~\ref{fig:MPI}.

\input{tablas/MPI-speedup}

\input{graficas/MPI}

De estos datos, primero debemos observar la nula escalabilidad al pasar de 1 a 2 procesos. Esto se debe a que con la versión secuencial se realizan muchas menos operaciones que con la paralela. De esta forma, los bloques de matrices de la versión paralela todavía son muy grandes y cada proceso tiene que realizar muchas más operaciones. Además, el proceso número 2 va a ser el que realice la mayoría de operaciones, por lo que es razonable que la ganancia sea prácticamente nula.
%Además, el coste de las comunicaciones entre procesos es realmente elevado debido al elevado tamaño de los bloques.

Sin embargo, y como podemos observar en los datos, el rendimiento del programa (en términos de escalabilidad) es realmente bueno cuando aumentamos el número de procesos. De forma que, si doblamos el número de hilos, el tiempo de ejecución de nuestro programa se va a reducir prácticamente a la mitad, puesto que se opera con bloques de matrices más pequeñas (aunque se asume que el coste de las comunicaciones aumentará en algún punto).

Es de esperar que al aumentar el número de procesos involucrados en la resolución del sistema se baje en eficiencia, puesto que la parte secuencial del código (la resolución mediante remonte y descenso) conllevará una porción más importante de tiempo. Respecto a esto, también se debe mencionar que, una vez factorizada la matriz, este tiempo es mínimo, por lo que el grueso de la ejecución se centra en el cálculo de $L$ y $U$.